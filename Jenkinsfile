
pipeline {
  agent {
    kubernetes {
      cloud "kubernetes-dev-cluster"
      yamlFile "JenkinsPod.yaml"
    }
  }
  environment {
    PROJECT = "soshibby"
    APP_NAME = "jenkins-kubernetes-demo"
    BRANCH = getBranchName()
    IMAGE_REPOSITORY = "registry.gitlab.com/${PROJECT}/${APP_NAME}"
    IMAGE_TAG = "${BRANCH}.${env.BUILD_NUMBER}"
    DOCKER_REGISTRY = "registry.gitlab.com/${PROJECT}/${APP_NAME}"
  }
  stages {
    stage("Build and Push Docker Image") {
      steps {
        container("docker") {
          withCredentials([usernamePassword(credentialsId: 'gitlab-registry-auth', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
            sh "docker build -t ${IMAGE_REPOSITORY}:${IMAGE_TAG} ."
            sh "docker login ${DOCKER_REGISTRY} -u $DOCKER_USERNAME -p $DOCKER_PASSWORD"
            sh "docker push  ${IMAGE_REPOSITORY}:${IMAGE_TAG}"
          }
        }
      }
    }
    stage("Deploy To Dev") {
      when {
        expression {
          return env.GIT_BRANCH == "origin/master"
        }
      }
      steps {
        container("helm") {
          sh "helm init --client-only"
          sh "helm upgrade \
                --install \
                --set namespace=default \
                --set image.repository=${IMAGE_REPOSITORY} \
                --set image.tag=${IMAGE_TAG} \
                nginx-example \
                nginx"
        }
      }
    }
  }
}

def getBranchName() {
  if (env.GIT_BRANCH.split('/').length > 1) {
    return env.GIT_BRANCH.split('/')[1];
  } else {
    return env.GIT_BRANCH;
  }
}